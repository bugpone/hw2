package edu.sjsu.android.hw2scratch2;
import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import
        android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Animal> values;
    private MainActivity act;

    public MyAdapter(MainActivity act, List<Animal> animals){
        this.values = animals;
        this.act = act;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public View layout;
        public ImageView imgView;
        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtHeader = (TextView) v.findViewById(R.id.firstLine);
            imgView = (ImageView) v.findViewById(R.id.icon);

        }
    }
    public void add(int position, Animal item) {
        values.add(position, item);
        notifyItemInserted(position);
    }
    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Animal> myDataset) {
        values = myDataset;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void clicked(View v, Animal animal,int position){
        Log.d("DEBUG","CLICKED");
        Intent intent = new Intent(v.getContext(), DescActivity.class);
        intent.putExtra("desc",animal.getDesc());
        intent.putExtra("imgID",animal.getImgID());

        Log.d("MDEBUG", "position: " + position);

        if(position == values.size()-1) {
            act.confirmProceed(intent);
        }
        else{
            v.getContext().startActivity(intent);
        }

       // MyQuestionDialogFragment fragment = new MyQuestionDialogFragment();
       // fragment.show(fragment.getFragmentManager(),"oo");

    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Animal animal = values.get(position);
        holder.txtHeader.setText(animal.getName());
        holder.txtHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked(v,animal,position);
            }
        });
        holder.imgView.setImageResource(animal.getImgID());
        holder.imgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked(v,animal,position);
            }
        });


    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }
}
