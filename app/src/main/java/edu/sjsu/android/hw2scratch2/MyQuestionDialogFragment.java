package edu.sjsu.android.hw2scratch2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class MyQuestionDialogFragment extends DialogFragment {
    Intent intent;
    public MyQuestionDialogFragment(Intent intent){
        this.intent = intent;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("WARNING: INFOHAZARD PRESENT \n\n If not handled properly, this file will not only kill you, it will hurt the whole time you're dying. \n\n Proceed?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Cry alone in a cold dark room

                    }
                });
        return builder.create();
    }
}
