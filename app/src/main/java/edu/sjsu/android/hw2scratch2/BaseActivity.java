package edu.sjsu.android.hw2scratch2;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

//we want dem all to have action bars bb
public class BaseActivity extends AppCompatActivity {



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d("MDEBUG","CLICKED");
        int id = item.getItemId();
        if(id == R.id.moreInfo){
            Log.d("MDEBUG","MORE INFO");
            Intent intent = new Intent(this, MoreInfoActivity.class);
            startActivity(intent);

        }

        if(id == R.id.uninstall){
            Log.d("MDEBUG","UNINSTALLING");
            Uri packageUri = Uri.parse("package:edu.sjsu.android.hw2scratch2");
           // packageUri = Uri.parse("package:$packageName");
            Intent intent = new Intent(Intent.ACTION_DELETE,packageUri);
            startActivity(intent);

        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        Log.d("DEBUG","OPTIONS MENU");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }


}
