package edu.sjsu.android.hw2scratch2;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class DescActivity extends BaseActivity {

    public TextView descBox;
    public ImageView imgView;
   // public RelativeLayout relativeLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        String desc = getIntent().getStringExtra("desc");
        int imgID = getIntent().getIntExtra("imgID",-699);
        Log.d("DEBUG",desc);
        descBox = (TextView) findViewById(R.id.textView);
        descBox.setText(desc);
        //descBox.setMovementMethod(new ScrollingMovementMethod());

        imgView = (ImageView) findViewById(R.id.imageView);
        imgView.setImageResource(imgID);

        //relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
       // relativeLayout.setMovementMethod(new ScrollingMovementMethod());



    }

}
