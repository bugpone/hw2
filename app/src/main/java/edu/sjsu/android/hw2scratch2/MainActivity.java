package edu.sjsu.android.hw2scratch2;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private MyReceiver reciever;

    //https://www.youtube.com/watch?v=oh4YOj9VkVE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        reciever = new MyReceiver();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to
        // improve performance if you know that changes
        // in content do not change the layout size
        // of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        final List<Animal> input = new ArrayList<>();
        //all these item descriptions were from the SCP wiki.
        input.add(new Animal ("rock. ","A rock that makes you procrastinate. I'll finish writing this up later.",R.drawable.animal1));
        input.add(new Animal ("A spear","A spear that, when thrown, pierces the heart of the nearest humanoid and extends several spikes from its blade afterward. Agents are to note that \"the nearest humanoid\" is typically the person who threw it.",R.drawable.animal2));
        input.add(new Animal ("A sculpture","An abstract bronze sculpture, measuring 2.11189 meters in height. Artist unknown. Exposure results in drastic overestimation of one's ability to make precise measurements for 9.800419 hours afterwards.",R.drawable.animal3));
        input.add(new Animal ("A rubber ducky"," A yellow \"rubber ducky\" bath toy. When a subject explains in detail a practical problem to the item as though it were a living anthropomorphic duck, they will feel that they have a better understanding of said problem, and are often immediately able to come up with a solution.",R.drawable.animal4));
        input.add(new Animal ("Singing cragglewood tree","An uncontainable idea - a recurring childhood memory of a non-existent theme-park ('Cragglewood Park'), estimated in 0.05% of the world's population. Subjects report being between the ages of 4 and 12 at the time of visiting. Notably, it appears to be most common among adults raised as a single, neglected child. \n\nThe image above was recovered from a mini video cassette found in the possession of Randolph Blair. The word 'CRAGGLEWOOD' is written across its label in black felt tip marker.\n\n",R.drawable.animal5));
        mAdapter = new MyAdapter(this,input);
        recyclerView.setAdapter(mAdapter);

    }

    public void confirmProceed(Intent intent) {
        DialogFragment newFragment = new MyQuestionDialogFragment(intent);
        newFragment.show(getSupportFragmentManager(), "proceed?");
    }

}