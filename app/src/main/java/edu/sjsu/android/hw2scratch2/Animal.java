package edu.sjsu.android.hw2scratch2;

public class Animal {
    String name = "default";
    String desc = "default";
    int imgID = -69;

    public Animal(String name, String desc, int imgID) {
        this.name = name;
        this.desc = desc;
        this.imgID = imgID;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getImgID() {
        return imgID;
    }

}
